#purpose here is video capture / creation from region
#OpenCV will display a window, you adjust the size of the window and then respond
#to the input request in the terminal by pressing any key.
#The script will then get the window boundaries and close the window,
#followed by beginning to record that section of the screen at the given
#framerate.

import cv2
import numpy as np
import av
import mss
from PIL import ImageChops
from pynput import mouse
from pynput import keyboard
from time import sleep

#Set up ffmpeg container / av stuff
fps = 24

global xy1, xy2, container, stream, quitting
xy1 = False
xy2 = False
container = False
stream = False
quitting = False


print("Use the mouse to click on upper left corner and lower right corner of recording region desired.")

#Capture the screen region upper left and lower right corners:
def on_click(x, y, button, pressed):
    global xy1, xy2
    if button == mouse.Button.left and not pressed:
        if xy1 is False:
            print('setting xy1 to ({}, {})'.format(x, y))
            xy1 = (x, y)
        elif xy2 is False and (x, y) != xy1:
            print('setting xy2 to ({}, {})'.format(x, y))
            xy2 = (x, y)
            return False;

mouseListener = mouse.Listener(on_click=on_click)
mouseListener.start()
mouseListener.join()

def on_release(key):
    global xy1, xy2, container, stream, quitting
    try:
        print('{} released'.format(key))
        if key.char == 'f':
            print('creating video - press c to finish the video')
            container = av.open('test.mp4', mode='w')
            stream = container.add_stream('mpeg4', rate=fps)
            stream.width = xy2[0] - xy1[0]
            stream.height = xy2[1] - xy1[1]
            stream.pix_fmt = 'yuv420p'
        elif key.char == 'c':
            quitting = True
            
    except ValueError:
        print("ValueError")
    except AttributeError:
        print("AttributeError")

print('region corners recorded, press "f" to begin')
keyboardListener = keyboard.Listener(on_release=on_release)
keyboardListener.start()

with mss.mss() as sct:
    while True:
        print("Waiting for f to begin")
        while container is not False:
            boundaries = (xy1[0], xy1[1], xy2[0], xy2[1])
            print(boundaries)
            print("Container created, starting")
            while quitting is not True:
                capturedImage = np.array(sct.grab(boundaries))
                image = capturedImage[:,:,:3]
                print(image.shape)
                frame = av.VideoFrame.from_ndarray(image, format='rgb24')
                for packet in stream.encode(frame):
                    container.mux(packet)
                sleep(fps/1000)
            print("Quitting")
            print('finishing video')
            for packet in stream.encode():
                container.mux(packet)
            container.close()
            container = False
            print("Done")
