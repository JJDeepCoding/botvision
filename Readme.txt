The purpose of this repo is to provide a fast way to grab each frame in a video stream, do something to it, and show the result
in another window (on Ubuntu).  This is useful for coders interested in deep learning and running any type of image operations
on video in order to see the result in real time.  Currently the only operation I have being done is a blur.  This is mostly
a template to show basic setup of the loop and my current environment setup process to quickly install everything required.

Currently using Anaconda3.  This could probably work on Windows, but in that case all the package install directions will be different and you would not need the gtk modules, etc.

Commands to start Anaconda (at the beginning):
(I answered "no" on installer - "do you want to activate..." next time yes)

Command1:  source <anaconda install directory>/bin/activate
Command2:  conda init

Close and restart shell (terminal:  ctrl-alt-t for new shell)

To activate / deactivate environment:
conda activate project1
conda deactivate

Link to conda cheatsheet (most useful commands):
https://docs.conda.io/projects/conda/en/4.6.0/_downloads/
52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf

How to install / basic usage of opencv in python:
https://docs.opencv.org/master/d2/de6/tutorial_py_setup_in_ubuntu.html

How I did it:
conda create -n project1 python=3.6
conda install -c menpo opencv3
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module   (this in order for the cv2.namedWindow()
	function to work properly)
conda install -c anaconda pillow    (PIL discontinued support in 2011, this was forked to continue support)
	(has very useful submodule ImageChops, which can quickly find differences between images and give the region
		boundaries)
conda install jupyter
conda install tensorflow-gpu
conda install -c conda-forge pyautogui  #for controlling keyboard and mouse + has alternate function for screenshots
conda install -c conda-forge python-mss  #supposedly fastest library for grabbing screenshots

pip install pynput  #using this for the keyboard listener - the python keyboard module requires root, which I didn't like, so using this instead

---------------------------
Note:  For the imageops (OpenCV 101 - image operations), to get the imutils module, you can do "pip install imutils".
The source is here:  https://github.com/jrosebr1/imutils

