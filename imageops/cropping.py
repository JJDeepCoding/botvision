import cv2
import numpy as np

def cropping(image, startX, endX, startY, endY):
    return image[startY:endY, startX:endX] 

image = cv2.imread("scrn.png")
image2 = cropping(image, 0, 10, 0, 20)
print(image2.shape)
