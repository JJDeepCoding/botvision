import cv2
import numpy as np
import imutils
from matplotlib import pyplot as plt

#For resizing, you'll often want to keep the aspect ratio, though if you are running the
#images through a neural net (and therefore need to keep the same image sizes for them all)
#you can most often afford to ignore aspect ratio and let them get a bit off.

#ratio = width/height.  CV2 default image format is height, width - or (y, x)
def findAspectRatio(image):
    return image.shape[1] / image.shape[0]

def adjustHeight(image, newHeight):
    #have to adjust width, so first we calculate the old aspect ratio (there may be another line to use here first, see the resizing folder)
    heightPercent = newHeight / image.shape[0]
    dim = (int(image.shape[1] * heightPercent), newHeight)
    return dim

def adjustWidth(image, newWidth):
    #have to adjust height by the ratio
    widthPercent = newWidth / image.shape[1]
    dim = (newWidth, int(image.shape[0] * widthPercent))
    return dim

def pltshow(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

#Also when resizing, you will need to choose the kernel algorithm.
#Here are the possible algorithms - see below them for more details about them:
#cv2.INTER_NEAREST  --> simplest algorithm
#cv2.INTER_LINEAR  --> best choice for compromise between speed/quality
#cv2.INTER_AREA 
#cv2.INTER_CUBIC
#cv2.INTER_LANCZOS4

#Best for increasing image size:
#cv2.INTER_LINEAR --> slightly faster than INTER_CUBIC
#cv2.INTER_CUBIC

#Best for decreasing image size:
#cv2.INTER_AREA  --> best for this
#cv2.INTER_NEAREST

#INTER_LINEAR is best default for both if you don't want to look things up.

#Get original image
original = cv2.imread("scrn.png")
print(original.shape)
print(findAspectRatio(original))

#resize smaller  #let's take 100 off width from original, then 100 off height from original, displaying both after resize
newWidth = original.shape[1] - 500
dim = adjustWidth(original, newWidth)
decreasedWidthImage = cv2.resize(original, dim, interpolation=cv2.INTER_AREA)
pltshow("image width reduced 500", decreasedWidthImage)
print(findAspectRatio(decreasedWidthImage))

newHeight = original.shape[0] - 500
dim = adjustHeight(original, newHeight)
decreasedHeightImage = cv2.resize(original, dim, interpolation=cv2.INTER_AREA)
pltshow("image height reduced 500", decreasedHeightImage)
print(findAspectRatio(decreasedHeightImage))

#resize larger
newWidth = original.shape[1] + 500
dim = adjustWidth(original, newWidth)
increasedWidthImage = cv2.resize(original, dim, interpolation=cv2.INTER_AREA)
pltshow("image width increased 500", increasedWidthImage)
print(findAspectRatio(increasedWidthImage))

newHeight = original.shape[0] + 500
dim = adjustHeight(original, newHeight)
increasedHeightImage = cv2.resize(original, dim, interpolation=cv2.INTER_AREA)
pltshow("image height increased 500", increasedHeightImage)
print(findAspectRatio(increasedHeightImage))

