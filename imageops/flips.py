import cv2
from matplotlib import pyplot as plt
import argparse

def parse(shorta, longa, help):
    ap = argparse.ArgumentParser()
    ap.add_argument(shorta, longa, type=str, help=help)
    args = vars(ap.parse_args())
    return args

def imread(image):
    return cv2.imread(image)
   
def plt_imshow(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()
    
def plotImage(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

#flipType:
#0 - vert
#1 - horiz
#-1 - both
def flip(image, flipType):
    return cv2.flip(image, flipType)

def flipVert(image):
    return flip(image, 0)

def flipHoriz(image):
    return flip(image, 1)

def flipHorizVert(image):
    return flip(image, -1)

args = parse("-i", "--image", "path to image")
print(args)
image = imread(args["image"])
print(image.shape)

vertImage = flipVert(image)
print(vertImage.shape)
plt_imshow("vertical flip", vertImage)

horizImage = flipHoriz(image)
plt_imshow("horiz flip", horizImage)

flipHorizVert = flipHorizVert(image)
plt_imshow("horiz and vert flip", flipHorizVert)

