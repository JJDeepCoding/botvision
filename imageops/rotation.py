import cv2
from matplotlib import pyplot as plt
import argparse
import imutils

def parse(shortArg, longArg, required, help):
    ap = argparse.ArgumentParser()
    ap.add_argument(shortArg, longArg, type=str, required=required, help=help)
    args = vars(ap.parse_args())
    return args

def imread(imageName):
    return cv2.imread(imageName)

def imwrite(imageName):
    cv2.imwrite(imageName)

def rotateImage(image, x, y, degrees, scale):
    (height, width) = image.shape[:2]
    M = cv2.getRotationMatrix2D((x, y), degrees, scale)
    return cv2.warpAffine(image, M, (width, height))    

def plotImage(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

args = parse("-i", "--image", True, "path to image")
print(args)
image = imread(args["image"])
print(image.shape)
imageXmiddle = image.shape[1]/2
imageYmiddle = image.shape[0]/2
rightRot90 = rotateImage(image, imageXmiddle, imageYmiddle, -90, 1)
plotImage("right90", rightRot90)
leftRot90 = rotateImage(image, imageXmiddle, imageYmiddle, 90, 1)
plotImage("left90", leftRot90)
rightRot180 = rotateImage(image, imageXmiddle, imageYmiddle, -180, 1)
plotImage("negativeRot180", rightRot180)
leftRot180 = rotateImage(image, imageXmiddle, imageYmiddle, 180, 1)
plotImage("leftRot180", leftRot180)
boundLeft90 = imutils.rotate_bound(image, 90)
plotImage("left90Bound", boundLeft90)
boundRight90 = imutils.rotate_bound(image, -90)
plotImage('right90Bound', boundRight90)
leftBound45 = imutils.rotate_bound(image, 45)
plotImage('left45bound', leftBound45)
rightBound45 = imutils.rotate_bound(image, -45)
plotImage('right45Bound', rightBound45)
