import cv2
import numpy as np
from matplotlib import pyplot as plt

#Masking involves using bitwise operations (AND, OR, NOT, etc).
#In order to do that, you provide an image and a mask.
#A mask is an image made up of a black background and a filled in shape (white color).
#In order to make the shape, you can use the drawing methods (drawing.py).
#In order to get the black background, you can create a numpy array filled with zeros (using the np.zeros method).

#Usage - you can use cv2 masking functions to remove part of an image (where the white shape is in the mask when the mask is superimposed over the image)
#and then replace that part with something else, or you can do the reverse, keep only the part of the image where the white shape is.

#This example just shows how to get the mask itself using drawing and bitwise operations

def imread(imagePath):
    return cv2.imread(imagePath)

#I will use circle and square masking in this example, but you should be able to find ways to use just about any filled-in shape.
def getBackground(width, height):
    return np.zeros((height, width), dtype="uint8")

def getSquareMask(backgroundImage, xStart, yStart, width, height):
    #the -1 fills the rectangle in, if it was a positive number, that would be the pixel thickness of the rectangle's border line
    #the 255 is the color white
    return cv2.rectangle(backgroundImage, (xStart, yStart), (xStart + width, yStart + height), 255, -1)

def getCircleMask(backgroundImage, x, y, radius):
    return cv2.circle(backgroundImage, (x, y), radius, 255, -1)

def pltshow(title, image, blackAndWhite):
    #this is a black and white image so the color operations aren't involved for the mask
    if blackAndWhite is False:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

imageBackground = getBackground(500, 500)
print(imageBackground.shape)
pltshow("background", imageBackground, True)

squareMask = getSquareMask(imageBackground, 200, 200, 100, 100)
pltshow("square mask", squareMask, True)

#note that changes to the imageBackground are kept on it from the getSquareMask function
#if you want a clean background, you have to get another background or overwrite it using getBackground()
imageBackground = getBackground(500, 500)
circleMask = getCircleMask(imageBackground, 250, 250, 60)
pltshow("circle mask", circleMask, True)

circleSquareAnd = cv2.bitwise_and(circleMask, squareMask)
pltshow("circle AND square", circleSquareAnd, True)

circleSquareNot = cv2.bitwise_not(circleSquareAnd)
pltshow("circle square NOT", circleSquareNot, True)

circleSquareOr = cv2.bitwise_or(circleMask, squareMask)
pltshow("circle OR square", circleSquareOr, True)

circleSquareXor = cv2.bitwise_xor(circleMask, squareMask)
pltshow("circle XOR square", circleSquareXor, True)

