import cv2
import argparse
from matplotlib import pyplot as plt
import numpy as np
import imutils

def argParse():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", type=str, default="scrn.png", help="path to image")
    ap.add_argument("-x", default=50)
    ap.add_argument("-y", default=50)
    return vars(ap.parse_args())

def imread(imagePath):
    return cv2.imread(imagePath)

def translate(image, xDelta, yDelta):
    M = np.float32([
        [1, 0, xDelta],
        [0, 1, yDelta]
    ])

    return cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))

def imtranslate(image, xDelta, yDelta):
    return imutils.translate(image, xDelta, yDelta)

#Apparently doesn't exist :(
#def imtranslateBound(image, xDelta, yDelta):
#    return imutils.translate_bound(image, xDelta, yDelta)
    
def pltshow(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

args = argParse()
image = imread(args["image"])
x = args["x"]
y = args["y"]
cv2Translate = translate(image, x, y)
imutilsTranslate = imtranslate(image, x, y)
#imutilsTranslateBound = imtranslateBound(image, x, y)

argsText = 'x: ' + str(x) + ', y: ' + str(y)
cv2translateTitle = 'cv2 translate, ' + argsText
imutilsTranslateTitle = 'imutils translate, ' + argsText
#imutilsTranslateBoundTitle = 'imutils translate bound, ' + argsText

pltshow(cv2translateTitle, cv2Translate)
pltshow(imutilsTranslateTitle, imutilsTranslate)
#pltshow(imutilsTranslateBoundTitle, imutilsTranslateBound)
