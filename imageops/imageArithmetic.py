import cv2
import numpy as np
from matplotlib import pyplot as plt

def pltshow(title, image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

def getOpArray(image, value):
    return np.ones(image.shape, dtype="uint8") * value

#Note that the cv2 add and subtract methods have safety so it doesn't wrap around (it maxes at 255 and mins at 0).  Numpy ones do not, so don't use those.
def imageAdd(image, M):
    return cv2.add(image, M)

def imageSubtract(image, M):
    return cv2.subtract(image, M)

image = cv2.imread("scrn.png")
print(image.shape)

opArray = getOpArray(image, 100)
print(opArray.shape)

brightened = imageAdd(image, opArray)
print(brightened.shape)
pltshow("brightened", brightened)

darkened = imageSubtract(image, opArray)
print(darkened.shape)
pltshow("darkened", darkened)
