import argparse
import cv2
from matplotlib import pyplot as plt
import numpy as np

def argParse():
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--image", required=True,
       help="path to input image")
    args = vars(ap.parse_args())
    return args
    
def imread(imageName):
    return cv2.imread(imageName)

def drawLine(canvas, x1, y1, x2, y2, color, filled):
    #(x,y) will be the starting point of the line, click again to set the other
    #side, c to cancel, s to save
    cv2.line(canvas, (x1,y1), (x2,y2), color, filled)

def drawRectangle(canvas, x1, y1, x2, y2, color, filled):
    #(x,y) will be the upper left corner, click again on lower right corner
    #c to cancel, s to save
    cv2.rectangle(canvas, (x1,y1), (x2,y2), color, filled)

def drawCircle(canvas, x, y, radius, color, filled):
    #(x,y) will be the center of the circle
    #move the mouse to expand the circle
    #c to cancel, s to save    
    #filled is either -1 for filled or a number for line thickness
    cv2.circle(canvas, (x, y), radius, color, filled)

def pltshow(title, image):
    plt.imshow(image)
    plt.title(title)
    plt.grid(False)
    plt.show()

canvas1 = np.zeros((500, 500), dtype="uint8")  #this would be black since they're all 0, and if it gets written on, it'll be grayspace values 0-255 or black and white 0 or 255.
drawLine(canvas1, 0, 0, 500, 500, 255, 1)
pltshow("line", canvas1)

canvas2 = np.zeros((500, 500), dtype="uint8") 
drawRectangle(canvas2, 200, 200, 400, 400, 255, 1)
pltshow("rectangle", canvas2)

canvas3 = np.zeros((500, 500), dtype="uint8") 
drawCircle(canvas3, 250, 250, 100, 255, 1)
pltshow("circle", canvas3)
