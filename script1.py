import cv2 as cv #for advanced image functions
import pyautogui as pg #for capturing screen and controlling mouse/keyboard
import numpy as np #for dealing with raw pixel data in arrays
from PIL import ImageChops #for screen differencing to find window boundaries
import time #to wait a second between screenshots
import mss
import mss.tools
import numpy
from pynput import keyboard #pynput.readthedocs.io/en/latest/keyboard.html

#global windowBoundaries:
boundaries = -1

#Find the position of the window with video stream.
def getCaptureBoundaries():
    #Capture screenshots (in png format):
    image = pg.screenshot() 
    time.sleep(1)
    image2 = pg.screenshot()

    #Next sequence is for locating changing pixels in order to find
    #the boundaries of the window I want to record (rest of screen
    #should not be changing in order for this to work).
    #https://stackoverflow.com/questions/189943/how-can-i-
    #quantify-difference-between-two-images   --> note: next answer contains some
    #nice algorithms like calculating "distance" between images
    diff = ImageChops.difference(image, image2)
    boundaries = diff.getbbox() #apparently it has all the functionality already!
    return boundaries

def setupWindow(boundaries, windowName):
    #x,y of top left corner of screen region with changing pixels:
    x1 = boundaries[0]
    y1 = boundaries[1]
    #x,y of bottom right corner of that screen region
    x2 = boundaries[2]
    y2 = boundaries[3]

    width = x2 - x1
    height = y2 - y1

    #move the new window to the left exactly the amount needed to show both
    cv.moveWindow(windowName, x1 - width, y1)

def refreshWindowPosition():
    windowBoundaries = getCaptureBoundaries()
    setupWindow(windowBoundaries, windowName)
    return windowBoundaries

#keyboard keypress detection:
def on_press(key):
    try:
        print('alphanumeric key {} pressed'.format(key.char))
    except AttributeError:
        print('special key {} pressed'.format(key))
        
def on_release(key):
    global boundaries
    try:
        print('{} released'.format(key))
        if key == keyboard.Key.esc:
            return false #this doesn't stop the main loop because it's in a diff thread
        elif key.char == 'f':
            boundaries = refreshWindowPosition()
            print("new boundaries {}".format(boundaries))
    except ValueError:
        print("oops")

#Detect keyboard events, non-blocking:
listener = keyboard.Listener(
        on_press=on_press,
        on_release=on_release)
listener.start()

#Setup window to show detections:
windowName = 'detections'
cv.namedWindow(windowName, cv.WINDOW_AUTOSIZE)
boundaries = refreshWindowPosition()

with mss.mss() as sct:
    while True:
        #take screenshot and show it
        time1 = time.perf_counter()
        capturedImage = numpy.array(sct.grab(boundaries))
        blurredCapture = cv.blur(capturedImage, (7, 7))
        #cv.imshow(windowName, capturedImage)
        cv.imshow(windowName, blurredCapture)
        time2 = time.perf_counter()
        frameCapTime = time2 - time1
        print("Frame cap, boundaries {} + display time: {} ".format(boundaries, frameCapTime)) 
        #if cv.waitKey(25) & 0xFF == ord('q'):
        #   break
        cv.waitKey(1)
